import org.junit.Test;

import static org.junit.Assert.*;

public class ContentHandlerTest {
    private double PriceToPay = 100.00;

    @Test
    public void checkout_NoDiscount() {
        double expected = PriceToPay * 1;

        ContentHandler CH = new ContentHandler(new NoDiscount());
        double actual = CH.checkout(PriceToPay);

        assertEquals(expected,actual, 0.0);
    }

    @Test
    public void checkout_StudentDiscount() {
        double expected = PriceToPay * 0.8;

        ContentHandler CH = new ContentHandler(new StudentDiscount());
        double actual = CH.checkout(PriceToPay);

        assertEquals(expected,actual, 0.0);
    }

    @Test
    public void checkout_TeacherDiscount() {
        double expected = PriceToPay * 0.6;

        ContentHandler CH = new ContentHandler(new TeacherDiscount());
        double actual = CH.checkout(PriceToPay);

        assertEquals(expected,actual, 0.0);
    }
}