import org.junit.Test;

import static org.junit.Assert.*;

public class ContentGuiceModuleTest {

    @Test
    public void getMensaDiscount_NoDiscount() {
        int PersonId = 0;
        ContentGuiceModule CGM = new ContentGuiceModule(PersonId);
        iDiscount discount = CGM.getMensaDiscount();

        assertTrue("Da die ID gleich Null ist, wird dieser Person kein Discount gewährt",discount instanceof NoDiscount);
    }

    @Test
    public void getMensaDiscount_StudentDiscount() {
        int PersonId = 140;
        ContentGuiceModule CGM = new ContentGuiceModule(PersonId);
        iDiscount discount = CGM.getMensaDiscount();

        assertTrue("Eine Person mit der ID über 100 is ein Student und sollte ein enprechendes Discount kriegen",discount instanceof StudentDiscount);
    }

    @Test
    public void getMensaDiscount_TeacherDiscount() {
        int PersonId = 41;
        ContentGuiceModule CGM = new ContentGuiceModule(PersonId);
        iDiscount discount = CGM.getMensaDiscount();

        assertTrue("Eine Person mit der ID > 0 bis einschließlich 100 is ein Lehrer und kriegt ein größeres Discount als ein Student",discount instanceof TeacherDiscount);
    }
}