import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class ContentGuiceModule extends AbstractModule {
    private int PersonID;

    public ContentGuiceModule(int id) {
        this.PersonID = id;
    }

    @Provides
    public iDiscount getMensaDiscount() {
        if(PersonID > 0 && PersonID <= 100){
            return new TeacherDiscount();
        }
        if(PersonID > 100){
            return new StudentDiscount();
        }
        if(PersonID == 0){
            return new NoDiscount();
        }
        return null;
    }
}
