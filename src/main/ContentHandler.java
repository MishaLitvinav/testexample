import com.google.inject.Inject;

public class ContentHandler {
    private final iDiscount discount;

    @Inject
    public ContentHandler(iDiscount discount) {
        this.discount = discount;
    }

    public double checkout(double price){
        double priceToPay = price - price * discount.getMensaDiscount();
        return priceToPay;
    }
}
