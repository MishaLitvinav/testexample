import com.google.inject.Guice;
import com.google.inject.Injector;

public class ApplicationMain {
    public static void main(String[] args) {
        //Als ein Außenstehender hat man die ID 0
        //Die ID's 1 bis 100 sind an das Personal vergeben
        //Die ID's ab 101 können die Studenten kriegen
        int ID = 0;

        Injector guice = Guice.createInjector(new ContentGuiceModule(ID));
        ContentHandler contenthandler = guice.getInstance(ContentHandler.class);
        double YourPrice = contenthandler.checkout(100);

        System.out.print("Zu zahlen : " + YourPrice+ " Euro");
    }
}
